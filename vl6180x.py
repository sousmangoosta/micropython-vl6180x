import time


class VL6180X:
    IDENTIFICATION__MODEL_ID = 0x000
    IDENTIFICATION__MODEL_REV_MAJOR = 0x001
    IDENTIFICATION__MODEL_REV_MINOR = 0x002
    IDENTIFICATION__MODULE_REV_MAJOR = 0x003
    IDENTIFICATION__MODULE_REV_MINOR = 0x004
    IDENTIFICATION__DATE_HI = 0x006
    IDENTIFICATION__DATE_LO = 0x007
    IDENTIFICATION__TIME = 0x008  # 16 - bit

    SYSTEM__MODE_GPIO0 = 0x010
    SYSTEM__MODE_GPIO1 = 0x011
    SYSTEM__HISTORY_CTRL = 0x012
    SYSTEM__INTERRUPT_CONFIG_GPIO = 0x014
    SYSTEM__INTERRUPT_CLEAR = 0x015
    SYSTEM__FRESH_OUT_OF_RESET = 0x016
    SYSTEM__GROUPED_PARAMETER_HOLD = 0x017

    SYSRANGE__START = 0x018
    SYSRANGE__THRESH_HIGH = 0x019
    SYSRANGE__THRESH_LOW = 0x01A
    SYSRANGE__INTERMEASUREMENT_PERIOD = 0x01B
    SYSRANGE__MAX_CONVERGENCE_TIME = 0x01C
    SYSRANGE__CROSSTALK_COMPENSATION_RATE = 0x01E  # 16 - bit
    SYSRANGE__CROSSTALK_VALID_HEIGHT = 0x021
    SYSRANGE__EARLY_CONVERGENCE_ESTIMATE = 0x022  # 16 - bit
    SYSRANGE__PART_TO_PART_RANGE_OFFSET = 0x024
    SYSRANGE__RANGE_IGNORE_VALID_HEIGHT = 0x025
    SYSRANGE__RANGE_IGNORE_THRESHOLD = 0x026  # 16 - bit
    SYSRANGE__MAX_AMBIENT_LEVEL_MULT = 0x02C
    SYSRANGE__RANGE_CHECK_ENABLES = 0x02D
    SYSRANGE__VHV_RECALIBRATE = 0x02E
    SYSRANGE__VHV_REPEAT_RATE = 0x031

    SYSALS__START = 0x038
    SYSALS__THRESH_HIGH = 0x03A
    SYSALS__THRESH_LOW = 0x03C
    SYSALS__INTERMEASUREMENT_PERIOD = 0x03E
    SYSALS__ANALOGUE_GAIN = 0x03F
    SYSALS__INTEGRATION_PERIOD = 0x040

    RESULT__RANGE_STATUS = 0x04D
    RESULT__ALS_STATUS = 0x04E
    RESULT__INTERRUPT_STATUS_GPIO = 0x04F
    RESULT__ALS_VAL = 0x050  # 16 - bit
    RESULT__HISTORY_BUFFER_0 = 0x052  # 16 - bit
    RESULT__HISTORY_BUFFER_1 = 0x054  # 16 - bit
    RESULT__HISTORY_BUFFER_2 = 0x056  # 16 - bit
    RESULT__HISTORY_BUFFER_3 = 0x058  # 16 - bit
    RESULT__HISTORY_BUFFER_4 = 0x05A  # 16 - bit
    RESULT__HISTORY_BUFFER_5 = 0x05C  # 16 - bit
    RESULT__HISTORY_BUFFER_6 = 0x05E  # 16 - bit
    RESULT__HISTORY_BUFFER_7 = 0x060  # 16 - bit
    RESULT__RANGE_VAL = 0x062
    RESULT__RANGE_RAW = 0x064
    RESULT__RANGE_RETURN_RATE = 0x066  # 16 - bit
    RESULT__RANGE_REFERENCE_RATE = 0x068  # 16 - bit
    RESULT__RANGE_RETURN_SIGNAL_COUNT = 0x06C  # 32 - bit
    RESULT__RANGE_REFERENCE_SIGNAL_COUNT = 0x070  # 32 - bit
    RESULT__RANGE_RETURN_AMB_COUNT = 0x074  # 32 - bit
    RESULT__RANGE_REFERENCE_AMB_COUNT = 0x078  # 32 - bit
    RESULT__RANGE_RETURN_CONV_TIME = 0x07C  # 32 - bit
    RESULT__RANGE_REFERENCE_CONV_TIME = 0x080  # 32 - bit

    RANGE_SCALER = 0x096  # 16 - bit - see STSW-IMG003 core/inc/vl6180x_def.h

    READOUT__AVERAGING_SAMPLE_PERIOD = 0x10A
    FIRMWARE__BOOTUP = 0x119
    FIRMWARE__RESULT_SCALER = 0x120
    I2C_SLAVE__DEVICE_ADDRESS = 0x212
    INTERLEAVED_MODE__ENABLE = 0x2A3

    def __init__(self, i2c, address=0x29):
        self.i2c = i2c
        self._address = address
        self.__scaling = 1
        self.__io_timeout = 0
        self.__did_timeout = False

    def init(self):
        if self.get_reg8(0x0016) != 1:
            # Recommended setup from the datasheet
            self.set_reg8(0x0207, 0x01)
            self.set_reg8(0x0208, 0x01)
            self.set_reg8(0x0096, 0x00)
            self.set_reg8(0x0097, 0xfd)
            self.set_reg8(0x00e3, 0x00)
            self.set_reg8(0x00e4, 0x04)
            self.set_reg8(0x00e5, 0x02)
            self.set_reg8(0x00e6, 0x01)
            self.set_reg8(0x00e7, 0x03)
            self.set_reg8(0x00f5, 0x02)
            self.set_reg8(0x00d9, 0x05)
            self.set_reg8(0x00db, 0xce)
            self.set_reg8(0x00dc, 0x03)
            self.set_reg8(0x00dd, 0xf8)
            self.set_reg8(0x009f, 0x00)
            self.set_reg8(0x00a3, 0x3c)
            self.set_reg8(0x00b7, 0x00)
            self.set_reg8(0x00bb, 0x3c)
            self.set_reg8(0x00b2, 0x09)
            self.set_reg8(0x00ca, 0x09)
            self.set_reg8(0x0198, 0x01)
            self.set_reg8(0x01b0, 0x17)
            self.set_reg8(0x01ad, 0x00)
            self.set_reg8(0x00ff, 0x05)
            self.set_reg8(0x0100, 0x05)
            self.set_reg8(0x0199, 0x05)
            self.set_reg8(0x01a6, 0x1b)
            self.set_reg8(0x01ac, 0x3e)
            self.set_reg8(0x01a7, 0x1f)
            self.set_reg8(0x0030, 0x00)
            self.set_reg8(self.SYSTEM__FRESH_OUT_OF_RESET, 0)

    def configure_default(self):
        # "Recommended : Public registers"

        # readout__averaging_sample_period = 48
        self.set_reg8(self.READOUT__AVERAGING_SAMPLE_PERIOD, 0x30)

        # sysals__analogue_gain_light = 6 (ALS gain = 1 nominal, actually 1.01 according to Table 14 in datasheet)
        self.set_reg8(self.SYSALS__ANALOGUE_GAIN, 0x46)

        # sysrange__vhv_repeat_rate = 255 (auto Very High Voltage temperature recalibration after every 255 range measurements)
        self.set_reg8(self.SYSRANGE__VHV_REPEAT_RATE, 0xFF)

        # sysals__integration_period = 99 (100 ms)
        # AN4545 incorrectly recommends writing to register 0x040; 0x63 should go in the lower byte, which is register 0x041.
        self.set_reg16(self.SYSALS__INTEGRATION_PERIOD, 0x0063)

        # sysrange__vhv_recalibrate = 1 (manually trigger a VHV recalibration)
        self.set_reg8(self.SYSRANGE__VHV_RECALIBRATE, 0x01)

        # "Optional: Public registers"

        # sysrange__intermeasurement_period = 9 (100 ms)
        self.set_reg8(self.SYSRANGE__INTERMEASUREMENT_PERIOD, 0x09)

        # sysals__intermeasurement_period = 49 (500 ms)
        self.set_reg8(self.SYSALS__INTERMEASUREMENT_PERIOD, 0x31)

        # als_int_mode = 4 (ALS new sample ready interrupt); range_int_mode = 4 (range new sample ready interrupt)
        self.set_reg8(self.SYSTEM__INTERRUPT_CONFIG_GPIO, 0x24)

        # Reset other settings to power-on defaults

        # sysrange__max_convergence_time = 49 (49 ms)
        self.set_reg8(self.SYSRANGE__MAX_CONVERGENCE_TIME, 0x31)

        # disable interleaved mode
        self.set_reg8(self.INTERLEAVED_MODE__ENABLE, 0)

        # reset range scaling factor to 1x
        # setScaling(1);

    def set_reg8(self, address, value):
        self.i2c.writeto(self._address, values)

    def set_reg16(self, address, value):
        self.i2c.writeto(self._address, values)

    def get_reg8(self, address):
        return values

    def get_reg16(self, address):
        return values

    # Starts continuous interleaved measurements with the given period in ms
    # (10 ms resolution; defaults to 500 ms if not specified). In this mode, each
    # ambient light measurement is immediately followed by a range measurement.
    #
    # The datasheet recommends using this mode instead of running "range and ALS
    # continuous modes simultaneously (i.e. asynchronously)".
    #
    # The period must be greater than the time it takes to perform both
    # measurements. See section 2.4.4 ("Continuous mode limits") in the datasheet
    # for details.
    def start_interleaved_continuous(self, period):
        period_reg = int((period / 10) - 1)
        period_reg = self.constrain(period_reg, 0, 254)

        self.set_reg8(self.INTERLEAVED_MODE__ENABLE, 1)
        self.set_reg8(self.SYSALS__INTERMEASUREMENT_PERIOD, period_reg)
        self.set_reg8(self.SYSALS__START, 0x03)

    # Stops continuous mode. This will actually start a single measurement of range
    # and/or ambient light if continuous mode is not active, so it's a good idea to
    # wait a few hundred ms after calling this function to let that complete
    # before starting continuous mode again or taking a reading.
    def stop_continuous(self):
        self.set_reg8(self.SYSRANGE__START, 0x01)
        self.set_reg8(self.SYSALS__START, 0x01)
        self.set_reg8(self.INTERLEAVED_MODE__ENABLE, 0)

    # Returns a range reading when continuous mode is activated
    # (readRangeSingle() also calls this function after starting a single-shot
    # range measurement)
    def read_range_continuous(self):
        millis_start = time.ticks_ms()

        while self.get_reg8(self.RESULT__INTERRUPT_STATUS_GPIO & 0x04) == 0:
            if self.__io_timeout > 0 and (time.ticks_ms() - millis_start) > self.__io_timeout:
                self.__did_timeout = True
                return 255

        range = self.get_reg8(self.RESULT__RANGE_VAL)
        self.set_reg8(self.SYSTEM__INTERRUPT_CLEAR, 0x01)
        return range

    # Returns an ambient light reading when continuous mode is activated
    # (readAmbientSingle() also calls this function after starting a single-shot
    # ambient light measurement)
    def read_ambient_continuous(self):
        millis_start = time.ticks_ms()
        while self.get_reg8(self.RESULT__INTERRUPT_STATUS_GPIO & 0x20) == 0:
            print(self.get_reg8(self.RESULT__INTERRUPT_STATUS_GPIO))
            if self.__io_timeout > 0 and (time.ticks_ms() - millis_start) > self.__io_timeout:
                self.__did_timeout = True
                return 0

        ambient = self.get_reg16(self.RESULT__ALS_VAL)
        self.set_reg8(self.SYSTEM__INTERRUPT_CLEAR, 0x02)

        return ambient

    # Did a timeout occur in one of the read functions since the last call to
    # timeoutOccurred()?
    def timeout_occurred(self):
        tmp = self.__did_timeout
        self.__did_timeout = False
        return tmp

    def set_timeout(self, timeout):
        self.__io_timeout = timeout

    def get_timeout(self):
        return self.__io_timeout

    def read_range_continuous_millimeters(self):
        return self.__scaling * self.read_range_continuous()

    def constrain(self, val, min_val, max_val):
        return min(max_val, max(min_val, val))
